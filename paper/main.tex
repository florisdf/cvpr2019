\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{acro}	% For abbreviations
\usepackage{cprotect}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Half-shot face recognition with clustered ArcFace descriptors}

\author{Floris De Feyter\\
KU Leuven --- EAVISE\\
Oude Markt 13, Leuven, Belgium\\
{\tt\small floris.defeyter@kuleuven.be}}

\maketitle
%\thispagestyle{empty}

\DeclareAcronym{svm}{%
    short = SVM,
    long = Support Vector Machine,
    class = nomencl,
    short-indefinite = an,
    long-indefinite = a
}

\DeclareAcronym{rbf}{%
    short = RBF,
    long = Radial Basis Function,
    class = nomencl,
    short-indefinite = an,
    long-indefinite = a
}

\DeclareAcronym{dbscan}{%
    short = DBSCAN,
    long = Density-Based Spatial Clustering of Applications with Noise,
    class = nomencl,
    short-indefinite = a,
    long-indefinite = a
}

\DeclareAcronym{ari}{%
    short = ARI,
    long = Adjusted Rand Index,
    class = nomencl,
    short-indefinite = an,
    long-indefinite = an
}

\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

%%%%%%%%% ABSTRACT
\begin{abstract}
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
In recent years, facial recognition systems have made significant progress in 
accuracy~\cite{schroff2015facenet}. This is mainly due to the rise of deep 
learning techniques that came along with an increase in computing power, along 
with the public availability of large face datasets. Moreover, current face 
datasets contain faces in less and less constrained settings.  This creates 
opportunities for applying facial recognition in true real-world settings.

\subsection{Individualized yearbooks}
In our use case, we employ facial recognition in composing individualized 
school yearbooks for children. Many teachers take photographs during the course 
of a school year. At the end of the school year, these images are made 
available to the parents. However, most parents are only interested in photos 
in which their child is present. Instead of letting them painstakingly go 
through each individual image, we offer an automated solution where a parent 
only receives the relevant photos.
The relevant images are found based on a \emph{reference} image that was 
uploaded by a parent of the child. Naturally, this use case puts current facial 
recognition systems to the test. The main challenges can be summarized as 
follows:
\begin{enumerate}
    \item The dynamic environment causes many faces to be blurry, occluded and/or
        non-frontal;
    \item To enhance user-friendliness, we only want to ask parents for a 
        \emph{single} reference image per child;
    \item Not every identity in the dataset has a reference image at all.
\end{enumerate}
Figure~\ref{fig:jadeplus_samples} shows randomly selected samples from our 
dataset, cropped around the face annotations (green boxes). It is obvious that 
the images are truly unconstrained in terms of facial expression, face angle, 
blur, resolution and lighting. The dataset contains 1629 images with 29 
identities.
\begin{figure}
    \includegraphics[width=.5\textwidth]{img/jadeplus_samples.pdf}
    \caption{Random samples from our dataset, cropped around the face 
    annotations (green boxes).}\label{fig:jadeplus_samples}
\end{figure}
In this paper, we show how to tackle the three challenges enumerated above by 
(a) employing a ResNet-100~\cite{he2016deep} trained with ArcFace
loss~\cite{deng2018arcface} for facial feature extraction; (b) classifying the 
deep features with \iac{svm}~\cite{cortes1995support,suykens1999least} 
classifier on inference; (c) clustering the dataset with the 
\ac{dbscan}~\cite{ester1996density} algorithm to create reference images for 
children that lack one.

\subsection{Deep facial feature extraction}

\subsection{\acs{svm} classification}
\acreset{svm}
Once the reference faces from the gallery are converted into deep facial
feature vectors, we can start classifying query faces. A typical approach is to
classify the queries by searching for the nearest neighbor in the gallery, 
i.e.\ by converting the query face into a feature vector and searching for the 
gallery vector to which the query vector has the smallest distance (e.g.\ 
Euclidean).

This distance-based approach, however, is not the sole way to classify a 
descriptor. We propose to perform the classification by employing \iac{svm}.  
\Iac{svm} creates linear decision boundaries between two classes in a 
high-dimensional feature space. The boundaries are created such that they
maximize the functional margin between two classes. This leads to an 
optimization problem that yields the classifier
\begin{equation}
    \widetilde{y}(\vec{x}) = \text{sign}\big[ \sum_{i=1}^{N_{sv}} \alpha_i y_i 
        K(\vec{x},\vec{x}_i)
    + b\big],
\end{equation}
where $\vec{x}_i \in \mathbb{R}^n$ is the \mbox{$i$-th} support vector;
$y_i \in \{-1, 1\}$ is the class label of the \mbox{$i$-th} support vector; 
$\widetilde{y}$ is the predicted class; $\alpha_i$ is a component of the
sparse vector $\boldsymbol{\alpha}$ which originates from solving the
optimization problem with Lagrangian multipliers; $K(\vec{x}_i, \vec{x}_j)$ is 
the kernel; and $N_{sv}$ is the number of support vectors. The kernel can be 
explicitly defined as a dot product between two vectors that are each 
transformed using a function $\phi(\cdot): \mathbb{R}^n \rightarrow 
\mathbb{R}^{n_h}$, or implicitly as a function mapping two vectors to a scalar.  
One example of such an implicit mapping is the \ac{rbf} kernel:
\begin{equation}
    K(\vec{x}, \vec{x}_i) = \exp{(-\gamma\norm{\vec{x} -
    \vec{x}_i}^2)},
\end{equation}
where $\gamma$ is a parameter controlling the influence of the support vectors.
A higher $\gamma$ means a quicker influence drop in the neighborhood of a
support vector.

\subsection{\acs{dbscan} clustering}
The \ac{dbscan} algorithm defines clusters as areas of high density separated 
by areas of low density. The algorithm has two parameters: $\epsilon$ and 
\verb|min_samples|. When at least \verb|min_samples| samples are within a 
distance $\epsilon$ of a sample (including the sample itself), that sample is 
considered a \emph{core sample}.  The samples around a core sample can be 
either themselves core samples or non-core samples. A cluster contains at least 
one core sample. Samples that are neither a core sample nor a non-core sample 
do not belong to a cluster and are considered outliers. The $\epsilon$ and 
\verb|min_samples| parameters determine the density of the clusters.

\section{Approach}
\begin{figure*}
    \centering
    \includegraphics{img/approach.pdf}
    \caption{Overview of the approach of creating an augmented reference set 
    using references induced from \ac{dbscan} clustering.}
\end{figure*}
\subsection{Feature clustering}
It is unfeasible to make a realistic a priori estimation of the amount of 
separate identities that are present in a dataset like ours. Therefore, a 
clustering algorithm like K-means clustering is inapplicable for the current 
use case as it takes the number of clusters as an argument. We propose to use 
the \ac{dbscan} clustering algorithm.

As the deep facial features are trained to maximize cosine similarity, we use 
this as the distance measure. 

\begin{enumerate}
    \item Cluster all queries, along with known references;
    \item Clusters with known references are discarded;
    \item Other clusters: add most central descriptor to reference set.
    \item Extra: discard near-duplicate references by looking for outliers in 
        terms of mean cosine distance to other references.
\end{enumerate}

\section{Results}
\subsection{Deep feature extraction}
Recent research in deep feature extraction for facial recognition hints towards 
using an angle-based loss during training instead of a Euclidean-based loss 
(e.g.~triplet loss). We evaluated networks of both kinds on our dataset. The 
first network is included in the Dlib library and consists of a network trained 
on a merge of multiple datasets with triplet loss.  
Figure~\ref{fig:pr_dlib_vs_inface} shows a comparison of the PR-curve when 
using Dlib features and the PR-curve for using InsightFace features. Both 
features were classified with Euclidean distance as a metric. It is obvious 
that the performance of InsightFace significantly exceeds the performance of 
Dlib. With the InsightFace features, the highest F1-score was achieved for 
$97.9\%$ precision $83.4\%$ recall, while the maximal F1-score for the Dlib 
features was for $64.7\%$ precision and $52.7\%$ recall.

\begin{figure}
    \includegraphics[width=.5\textwidth]{img/pr_descr_compare.pdf}
    \caption{PR curves for Dlib and InsightFace facial feature extraction using 
    a Euclidean distance-based classification.}\label{fig:pr_dlib_vs_inface}
\end{figure}

\subsection{\acs{svm} classification}
\subsubsection{Tuning the hyperparameters}
\Iac{svm} has multiple hyperparameters that need to be tuned before the 
\ac{svm} can be used. More particularly, an optimal value for $C$ and $\gamma$ 
needs to be found. $C$ trades off correct classifications with the maximization 
of the decision function's margin. When $C$ is large, there will be less 
tolerance for wrong classifications and the decision function's margin will be 
smaller, with a higher chance of overfitting the training set. A smaller $C$ 
will lead to more wrong training classifications, but a larger decision margin.
The $\gamma$ hyperparameter controls the amount of influence a support vector 
has on its surroundings. The larger $\gamma$, the quicker the influence drops 
as a function of distance to the support vector.

We performed a grid search of both hyperparameters, letting $C$ vary 
logarithmically between $10^0$ and $10^5$; and $\gamma$ vary logarithmically 
between $10^{-5}$ and $10^{0}$. The deep features that were extracted from the 
dataset of school images, were split into a training ($70\%$) and test ($30\%$) 
set. The training set was split into $5$ folds that were used to carry out a 
5-fold cross validation scheme for all possible combinations of the values for 
$C$ and $\gamma$.  Figure~\ref{fig:gridsearch} displays the average validation 
scores for all hyperparameter values. As noted by many previous authors, there 
seems to be a diagonal line in the grid, i.e.~for intermediate values of $C$ 
and $\gamma$,  lower $\gamma$ demands larger values of $C$ for the same 
validation score.  Intuitively, this is due to a lower $\gamma$ decreasing the 
smoothness of the model and a higher $C$ increasing the complexity by demanding 
more correct training classifications.  From Figure~\ref{fig:gridsearch}, it 
can be seen that a proper choice for the hyperparameters would be $C=1.0$ and 
$\gamma = 0.1$. We will use these values for the comparison with a cosine 
similarity-based classifier, which would be the most obvious choice for the 
InsightFace features as the model was trained to maximize cosine similarity.

\begin{figure}
    \includegraphics[width=.5\textwidth]{img/grid_search.pdf}
    \caption{Average validation score for multiple values of the \ac{svm} 
    hyperparameters (higher means better).}\label{fig:gridsearch}
\end{figure}

\subsubsection{Deep feature classification}
Using the hyperparameters that followed from the grid search, we compared the 
difference in performance of the \ac{svm} with a cosine similarity-based 
classifier. The deep features being classified are those that were in the test 
set during the grid search and hence are independent of our choice of 
hyperparameters.
Figure~\ref{fig:pr_cos_vs_svm} displays a micro-averaged PR-curve for the
29 identities in our dataset.
Using \iac{svm} instead of cosine similarity clearly improves the 
classification performance. The cosine similarity-based classifier achieves a 
maximal F1-score for $96.4\%$ precision
and $82.4\%$ recall, while the \ac{svm}-based classifier achieves a maximal
F1-score at $96.3\%$ precision and $92.5\%$ recall, i.e.\ about $10\%$ more 
recall for the same precision.

\begin{figure}
    \includegraphics[width=.5\textwidth]{img/pr_clf_compare.pdf}
    \caption{PR curves for cosine similarity-based and \ac{svm}-based 
    classification.}\label{fig:pr_cos_vs_svm}
\end{figure}

\subsection{Reducing the number of known classes}
One of the specific problems in the use case of creating yearbooks, is that we 
do not have access to a labeled reference image of children whose parents are 
not interested in buying our yearbooks. More generally, this problem demands to 
add elements of unsupervised learning to the supervised task of retrieving 
images of known children. We tackled this problem by employing a clustering 
algorithm to explore the identities in the dataset before starting the 
classification task. We evaluated the performance of this approach for multiple 
numbers of unknown faces.

\subsubsection{\acs{dbscan} grid search}
The values to choose for the parameters $\epsilon$ and \verb|min_samples| for 
the \ac{dbscan} clustering algorithm depend on the density of the data to 
cluster. Therefore, we performed a grid search to find the parameter values 
that fit our data the best. We did this by employing the same training set that 
was used for the \ac{svm} grid search. To evaluate the performance of the 
clustering, we used two metrics: the \ac{ari} and the number of clusters found.
The \ac{ari} is based on the Rand Index, which is a metric to determine the 
similarity of two clusterings. The \ac{ari}, however, is adjusted for chance 
such that random clusterings will have \iac{ari} close to $0$.  When two 
clusterings are identical up to a permutation, the \ac{ari} will be $1$.  
Figure~\ref{fig:dbscan_gs_ari} shows the \ac{ari} between the \ac{dbscan} 
clustering and the true labels for multiple combinations of $\epsilon$ and 
\verb|min_samples|. While the highest \ac{ari} scores are obtained in the 
leftmost column of the heat map, setting \verb|min_samples| to $1$ would not be 
a good choice as is apparent from Figure~\ref{fig:dbscan_gs_nclust} which shows 
that the number of clusters becomes very high in that case. Ideally, the number 
of clusters would be equal to 29 (+ 1 for outliers) corresponding to the 29 
identities present in the dataset. The color map in 
Figure~\ref{fig:dbscan_gs_nclust} is centered around 30, such that the darkest 
areas are those with the correct amount of clusters. When comparing 
Figs.~\ref{fig:dbscan_gs_ari}~and~\ref{fig:dbscan_gs_nclust}, a reasonable 
choice for the parameters would be $\epsilon = 0.42$ and 
$\verb|min_samples|=5$.

\begin{figure}
    \includegraphics[width=.5\textwidth]{img/dbscan_gs_ari.pdf}
    \cprotect\caption{\ac{ari} between the \ac{dbscan} clustering and the true 
        labels for multiple combinations of $\epsilon$ and
    \verb|min_samples|; higher is better.}\label{fig:dbscan_gs_ari}
\end{figure}

\begin{figure}
    \includegraphics[width=.5\textwidth]{img/dbscan_gs_nclust.pdf}
    \cprotect\caption{Number of clusters according to the \ac{dbscan} 
    clustering for multiple combinations of $\epsilon$ and
    \verb|min_samples|; closer to $30$ is better.}\label{fig:dbscan_gs_nclust}
\end{figure}

\subsubsection{Classifying with cluster references}
To evaluate the extent to which clustering can help to find reference images of 
unknown children, we performed multiple experiments with two different 
\emph{gallery types}. The first type of gallery simply contains reference 
images that were uploaded by parents. This type of gallery, labeled 
\emph{``Only known refs''}, thus consists of images of a limited set of 
children and does not cover all the identities in the data set. The second type 
of gallery, labeled \emph{``Known + cluster refs''}, also contains the uploaded 
known references, but is augmented with references that were induced from 
clusters.
\begin{figure}
    \includegraphics[width=.5\textwidth]{img/map-vs-n_unkown_labels.pdf}
    \caption{Influence of the number of unknown identities on the mAP for 
    different gallery types.}\label{fig:map_vs_unknown}
\end{figure}
Evidently, the amount of \emph{``known references''} determines how well a 
gallery with only known references will perform. This can be seen on the green 
curve in Figure~\ref{fig:map_vs_unknown}. The more persons that do not have a 
reference image in the gallery, the lower the mAP of the classifier will be.  
This is because unknown identities will be forced to be classified into one of 
the known identities. These misclassifications can be partly prevented by using 
a proper threshold, but as the curve shows a loss in mAP is inevitable.
The margin around the curve is the standard deviation after 30 random 
selections of the known references.

A gallery that is augmented with references that are induced from clusters, 
will potentially improve the classification result by creating also classes for 
the unknown children. This improvement is confirmed by our experiments, as 
shown on the orange curve in Figure~\ref{fig:map_vs_unknown}. Surprisingly, 
\emph{more unknown} children leads to a \emph{higher} mAP when augmenting the 
known references with cluster-induced references. The cluster-induced 
references are taken from the centers of the clusters, and are therefore very 
good representations of the respective identities. The quality of the 
cluster-induced references can also be seen when comparing the galleries in 
Figure~\ref{fig:cluster_vs_manual_gallery} of Appendix~\ref{app:ref_imgs}. The 
topmost figure shows a case of 3 known children (shown in black). The orange 
images are references that were induced from clustering. The lowermost figure 
shows the same identities, but with references that were manually selected. One 
can see that all but one identities were correctly induces from the clustering.  
The child that was not found by clustering, has significantly less images than 
the other children (34 vs.\ an average of 93), and was considered an outlier by 
the clustering algorithm. As an illustration, Figure~\ref{fig:pr_3_known} shows 
the micro-averaged PR-curve for the 3 known children shown in black in 
Fig.~\ref{fig:cluster_vs_manual_gallery}.
\begin{figure}
    \includegraphics[width=.5\textwidth]{img/pr_pseudo_all_known.pdf}\label{fig:pr_3_known}
\end{figure}

\section{Conclusion}

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\appendix
\section{Reference images}\label{app:ref_imgs}
\begin{figure*}
    \centering
    \includegraphics[width=.6\textwidth]{img/gallery_pseudo.pdf}
    \includegraphics[width=.6\textwidth]{img/gallery_all.pdf}
    \caption{Above: gallery with 3 manually selected reference images (black) 
        and 25 cluster reference images found by using \ac{dbscan} (orange); 
    Below: gallery with 29 reference images that were selected 
manually.}\label{fig:cluster_vs_manual_gallery}
\end{figure*}
\end{document}
